package eu.epitech.cashmanager

import android.app.Application
import eu.epitech.cashmanager.model.OrdersRepository
import eu.epitech.cashmanager.model.PaymentMethod
import eu.epitech.cashmanager.model.PaymentMethod.*
import eu.epitech.cashmanager.model.Product
import eu.epitech.cashmanager.utils.ProductList

class CashManagerApp : Application() {

    private var _paymentMethod = NULL
    var productList: ProductList = ProductList()

    fun reset(){
        productList.initListProduct()
        _paymentMethod = NULL
    }

    override fun onCreate() {
        super.onCreate()
        reset()
        instance = this
        productList.initListProduct()
    }

    fun setPaymentMethod(paymentMethod: PaymentMethod) {
        OrdersRepository.getInstance().setPaymentMethod(paymentMethod) {}
        _paymentMethod = paymentMethod
    }

    fun getPaymentMethod():PaymentMethod{
        return _paymentMethod
    }

    companion object {
        lateinit var instance: CashManagerApp
        val currency:String = "EUR"

    }
}