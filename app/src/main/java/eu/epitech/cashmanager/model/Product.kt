package eu.epitech.cashmanager.model

data class Product (
    val id:String,
    var name:String,
    var desc:String,
    var image:String,
    var qty:Int,
    var price:Int
)