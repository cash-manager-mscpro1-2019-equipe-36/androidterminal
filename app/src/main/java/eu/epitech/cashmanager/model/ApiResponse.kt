package eu.epitech.cashmanager.model

data class CreateOrderRequest(
        var userName: String
)

data class SetPaymentMethodRequest(
        var currency: String,
        var paymentMethod: String
)

data class ProductRequest(
        var currency: String,
        var id: String,
        var name: String,
        var price: Int,
        var quantity: Int
)