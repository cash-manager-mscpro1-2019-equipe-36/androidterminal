package eu.epitech.cashmanager.model

import android.util.Log
import eu.epitech.cashmanager.CashManagerApp
import eu.epitech.cashmanager.service.api.ApiClient
import eu.epitech.cashmanager.service.api.ApiService
import eu.epitech.cashmanager.utils.Util
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrdersRepository {

    fun createOrder(userName: String, onResult: (isSuccess: Boolean, response: String?) -> Unit) {
        val apiClient = ApiClient.apiClient(ApiService::class.java)
        apiClient.createOrder(CreateOrderRequest(userName)).enqueue(object : Callback<String?> {
            override fun onResponse(call: Call<String?>?, response: Response<String?>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<String?>?, t: Throwable?) {
                println(t?.message)
                onResult(false, null)
            }
        })
    }

    fun addProduct(product: Product, onResult: (isSuccess: Boolean) -> Unit) {

        val apiClient = ApiClient.apiClient(ApiService::class.java)
        if (Util.getOrderId(CashManagerApp.instance) == "")
            onResult(false)

        apiClient.addProductToOrder(
            Util.getOrderId(CashManagerApp.instance).toString(),
            ProductRequest(CashManagerApp.currency, product.id, product.name, product.price, 1)
        ).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>?,
                response: Response<ResponseBody?>?
            ) {
                if (response != null && response.isSuccessful)
                    onResult(true)
                else
                    onResult(false)
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                println(t?.message)
                onResult(false)
            }

        })
    }

    fun removeProduct(product: Product, onResult: (isSuccess: Boolean) -> Unit) {

        val apiClient = ApiClient.apiClient(ApiService::class.java)
        if (Util.getOrderId(CashManagerApp.instance) == "")
            onResult(false)

        apiClient.removeProductToOrder(
            Util.getOrderId(CashManagerApp.instance).toString(),
            ProductRequest(CashManagerApp.currency, product.id, product.name, product.price, 1)
        ).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>?,
                response: Response<ResponseBody?>?
            ) {
                if (response != null && response.isSuccessful)
                    onResult(true)
                else
                    onResult(false)
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                println(t?.message)
                onResult(false)
            }

        })
    }

    fun setPaymentMethod(paymentMethod: PaymentMethod, onResult: (isSuccess: Boolean) -> Unit) {

        val apiClient = ApiClient.apiClient(ApiService::class.java)
        if (Util.getOrderId(CashManagerApp.instance) == "")
            onResult(false)

        apiClient.SetPaymentMethod(
            Util.getOrderId(CashManagerApp.instance).toString(),
            SetPaymentMethodRequest(CashManagerApp.currency, paymentMethod.toString())
        ).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>?,
                response: Response<ResponseBody?>?
            ) {
                if (response != null && response.isSuccessful)
                    onResult(true)
                else
                    onResult(false)
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                println(t?.message)
                onResult(false)
            }
        })
    }

    fun execute(onResult: (isSuccess: Boolean) -> Unit) {

        val apiClient = ApiClient.apiClient(ApiService::class.java)
        if (Util.getOrderId(CashManagerApp.instance) == "")
            onResult(false)

        apiClient.ExecutePayment(
            Util.getOrderId(CashManagerApp.instance).toString()
        ).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>?,
                response: Response<ResponseBody?>?
            ) {
                if (response != null && response.isSuccessful)
                    onResult(true)
                else
                    onResult(false)
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                println(t?.message)
                onResult(false)
            }
        })
    }

    fun healthcheck(onResult: (isSuccess: Boolean) -> Unit) {

        val apiClient = ApiClient.apiClient(ApiService::class.java)
        apiClient.healthcheck().enqueue(object : Callback<String?> {
            override fun onResponse(call: Call<String?>?, response: Response<String?>?) {
                if (response != null && response.isSuccessful)
                    onResult(true)
                else
                    onResult(false)
            }

            override fun onFailure(call: Call<String?>?, t: Throwable?) {
                println(t?.message)
                onResult(false)
            }
        })
    }


    companion object {
        private var INSTANCE: OrdersRepository? = null
        fun getInstance() = INSTANCE
            ?: OrdersRepository().also {
                INSTANCE = it
            }
    }
}