package eu.epitech.cashmanager.model

enum class PaymentMethod{
    NULL,
    CREDIT_CARD,
    CHEQUE
}