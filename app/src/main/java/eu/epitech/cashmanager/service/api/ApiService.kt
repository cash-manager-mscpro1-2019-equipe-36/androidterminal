package eu.epitech.cashmanager.service.api

import eu.epitech.cashmanager.model.CreateOrderRequest
import eu.epitech.cashmanager.model.ProductRequest
import eu.epitech.cashmanager.model.SetPaymentMethodRequest
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

  //  @GET("search/repositories")
    //fun getRepo(@Query("q") search: String = "trending", @Query("sort") sort: String = "stars"): Call<GitResponse>

    @GET("api/server/healthcheck")
    fun healthcheck(): Call<String?>

    @POST("api/orders/create")
    fun createOrder(@Body userName: CreateOrderRequest): Call<String?>

    @PUT("api/orders/{order_id}/add_line_item")
    fun addProductToOrder(@Path(value = "order_id", encoded = true) order_id: String, @Body productRequest: ProductRequest): Call<ResponseBody>

    @PUT("api/orders/{order_id}/remove_line_item")
    fun removeProductToOrder(@Path(value = "order_id", encoded = true) order_id: String, @Body productRequest: ProductRequest): Call<ResponseBody>

    @PUT("api/orders/{order_id}/set_payment_method")
    fun SetPaymentMethod(@Path(value = "order_id", encoded = true) order_id: String, @Body setPaymentMethodRequest: SetPaymentMethodRequest): Call<ResponseBody>

    @PUT("api/orders/{order_id}/execute")
    fun ExecutePayment(@Path(value = "order_id", encoded = true) order_id: String): Call<ResponseBody>

}