package eu.epitech.cashmanager.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import eu.epitech.cashmanager.R
import eu.epitech.cashmanager.model.OrdersRepository
import eu.epitech.cashmanager.utils.Popup
import eu.epitech.cashmanager.utils.Util
import kotlinx.android.synthetic.main.activity_auth.*
import okhttp3.*

class AuthActivity : AppCompatActivity() {

    internal lateinit var debugButton: Button
    internal lateinit var exitDebugButton: Button

    private val client = OkHttpClient()
    private var devNb = 10
    private var infoDebug: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        exitDebugButton = findViewById(R.id.exitDebugButton)
        if (Util.getDebugState(this)) {
            exitDebugButton.visibility = View.VISIBLE
        }
        findViewById<EditText>(R.id.ipEditText).setText(Util.getServerIP(this))
    }

    fun buttonConfig(view: View) {
        println(Util.getDebugState(this))
        Popup.createPopup("Fail Access to server",R.drawable.error,this)
    }

    fun buttonLogin(view: View) {
        var password : String = findViewById<EditText>(R.id.passwordEditText).text.toString()
        var email : String = findViewById<EditText>(R.id.emailEditText).text.toString()
        var ip : String = findViewById<EditText>(R.id.ipEditText).text.toString()
        Util.persistServerIP(ip, this)
        println(ip)
        if (Util.getDebugState(this)){
            val intent = Intent(this, ShopActivity::class.java)
            startActivity(intent)
            return
        }
        OrdersRepository.getInstance().healthcheck { isSuccess ->
            if (isSuccess) {
                OrdersRepository.getInstance().createOrder(email) { isSuccess, response ->
                    if (isSuccess) {
                        Util.persistOrderId(response.toString(),this)
                        if (login(email,password)){
                            val intent = Intent(this, ShopActivity::class.java)
                            startActivity(intent)
                            finish()
                        } else {
                            Popup.createPopup("Incorrect email/password",R.drawable.error,this)
                        }
                    }
                }
            }else {
                Popup.createPopup("Connection error",R.drawable.error,this)

            }
        }
    }

    fun login(email: String, password : String) : Boolean
    {
        if (email == "test" && password == "test"){
            return true
        }
        return false
    }

    // Debug Mode =====================================

    fun buttonDebug(view: View) {
        this.devNb--
        if (this.devNb == 5) {
            this.infoDebug = Toast.makeText(
                this.baseContext, this.devNb.toString() + " tap(s) left to enter test mode",
                Toast.LENGTH_SHORT
            )
            this.infoDebug?.show()
        }
        if (this.devNb > 0 && this.devNb < 5) {
            this.infoDebug?.cancel()
            this.infoDebug = Toast.makeText(
                this.baseContext, this.devNb.toString() + " tap(s) left to enter test mode",
                Toast.LENGTH_SHORT
            )
            this.infoDebug?.show()
        }
        if (this.devNb == 0) {
            this.infoDebug?.cancel()
            Toast.makeText(
                this.baseContext, "You are in debug mode now",
                Toast.LENGTH_LONG
            ).show()
            Util.persistDebugState(true, this)
            this.exitDebugButton.visibility = View.VISIBLE
        }
    }

    fun buttonExitDebug(view: View) {
        this.devNb = 10
        this.exitDebugButton.visibility = View.INVISIBLE
        Util.persistDebugState(false, this)
    }
}
