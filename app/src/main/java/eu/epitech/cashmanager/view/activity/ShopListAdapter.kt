package eu.epitech.cashmanager.view.activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import eu.epitech.cashmanager.CashManagerApp
import eu.epitech.cashmanager.R
import eu.epitech.cashmanager.model.Product


class ShopListAdapter (private var context: Context, var dataSource: ArrayList<Product>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ViewHolder

        if (convertView == null) {
            view = inflater.inflate(R.layout.shop_list_item, parent, false)
            holder = ViewHolder()
            holder.titleTextView = view.findViewById(R.id.title) as TextView
            holder.priceTextView = view.findViewById(R.id.price) as TextView
            holder.qtyTextView = view.findViewById(R.id.qty) as TextView
            holder.moreButton = view.findViewById(R.id.more_btn) as Button
            holder.lessButton = view.findViewById(R.id.less_btn) as Button
            holder.iconImageView = view.findViewById(R.id.icon) as ImageView
            view.tag = holder
            holder.moreButton.setOnClickListener{v : View ->
                var product = getItem(position) as Product
                CashManagerApp.instance.productList.addPoduct(position,parent?.context)
                holder.qtyTextView.text = product.qty.toString()
            }
            holder.lessButton.setOnClickListener{v : View ->
                var product = getItem(position) as Product
                CashManagerApp.instance.productList.subPoduct(position,parent?.context)
                holder.qtyTextView.text = product.qty.toString()
            }

        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }
        val titleTextView = holder.titleTextView
        val priceTextView = holder.priceTextView
        val moreButton = holder.moreButton
        val lessButton = holder.lessButton
        val iconImageView = holder.iconImageView
        val qtyTextView = holder.qtyTextView

        val product = getItem(position) as Product

        qtyTextView.text = product.qty.toString()
        titleTextView.text = product.name
        priceTextView.text = ((product.price * 0.01).toString() + " " + CashManagerApp.currency)
        Picasso.get().load(product.image).into(iconImageView)
        return view
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    fun getDataList() : ArrayList<Product> {
        return this.dataSource
    }


    private class ViewHolder {
        lateinit var titleTextView: TextView
        lateinit var descTextView: TextView
        lateinit var qtyTextView: TextView
        lateinit var priceTextView: TextView
        lateinit var moreButton: Button
        lateinit var lessButton: Button
        lateinit var iconImageView:ImageView
    }

}