package eu.epitech.cashmanager.view.activity
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import eu.epitech.cashmanager.CashManagerApp
import eu.epitech.cashmanager.R
import eu.epitech.cashmanager.model.Product


class CartListAdapter (private var context: Context, var dataSource: ArrayList<Product>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ViewHolder

        if (convertView == null  ) {
            view = inflater.inflate(R.layout.cart_list_item, parent, false)
            holder = ViewHolder()
            holder.titleTextView = view.findViewById(R.id.title) as TextView
            holder.priceTextView = view.findViewById(R.id.price) as TextView
            holder.qtyTextView = view.findViewById(R.id.qty) as TextView
            holder.totalTextView = view.findViewById(R.id.total) as TextView
            holder.iconImageView = view.findViewById(R.id.icon) as ImageView
            view.tag = holder
        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }
        val titleTextView = holder.titleTextView
        val priceTextView = holder.priceTextView
        val iconImageView = holder.iconImageView
        val qtyTextView = holder.qtyTextView
        val totalTextView = holder.totalTextView

        val product = getItem(position) as Product

        qtyTextView.text = product.qty.toString()
        titleTextView.text = product.name
        priceTextView.text = ((product.price * 0.01).toString() + " " + CashManagerApp.currency)
        totalTextView.text = ((((product.price * product.qty))* 0.01).toString() + " " + CashManagerApp.currency)
        Picasso.get().load(product.image).into(iconImageView)
        return view
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    fun getDataList() : ArrayList<Product> {
        return this.dataSource
    }


    private class ViewHolder {
        lateinit var titleTextView: TextView
        lateinit var qtyTextView: TextView
        lateinit var totalTextView: TextView
        lateinit var priceTextView: TextView
        lateinit var iconImageView:ImageView
    }

}