package eu.epitech.cashmanager.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import eu.epitech.cashmanager.CashManagerApp
import eu.epitech.cashmanager.R
import eu.epitech.cashmanager.model.PaymentMethod
import eu.epitech.cashmanager.model.Product
import eu.epitech.cashmanager.utils.Popup
import eu.epitech.cashmanager.utils.ProductList
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_shop.*

class CartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        val productList: ProductList = CashManagerApp.instance.productList
        val listAdapter = CartListAdapter(this, productList.getCartList())
        cart_list.adapter = listAdapter

        when(CashManagerApp.instance.getPaymentMethod()){
            PaymentMethod.CREDIT_CARD -> paymentMethod.text = getString(R.string.payment_method_credit_card)
            PaymentMethod.CHEQUE -> paymentMethod.text = getString(R.string.payment_method_cheque)
            PaymentMethod.NULL -> {
                paymentMethod.text = "NULL"
                Popup.createPopup("No Payment Method Selected", R.drawable.error, this)
            }
        }
        total.text = ((productList.getTotal()*0.01).toString() + " " + CashManagerApp.currency)

        pay_btn.setOnClickListener({v->
            val intent = Intent(this, PaymentActivity::class.java)
            startActivity(intent)
            finish()
        })
    }
}
