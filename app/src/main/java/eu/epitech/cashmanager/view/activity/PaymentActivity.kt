package eu.epitech.cashmanager.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import eu.epitech.cashmanager.CashManagerApp
import eu.epitech.cashmanager.R
import eu.epitech.cashmanager.model.OrdersRepository
import eu.epitech.cashmanager.model.PaymentMethod
import eu.epitech.cashmanager.utils.Popup
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_payment.*

class PaymentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        when(CashManagerApp.instance.getPaymentMethod()){
            PaymentMethod.CREDIT_CARD -> creditCardPayment()
            PaymentMethod.CHEQUE -> chequePayment()
            PaymentMethod.NULL -> {
                Popup.createPopup("No Payment Method Selected", R.drawable.error, this)
            }
        }
        OrdersRepository.getInstance().execute{ isSuccess ->
            progressBar.visibility = ProgressBar.GONE
            if (isSuccess) {
                payment_message_succes.visibility = View.VISIBLE
                Popup.createPopup("Valid Payment", R.drawable.ic_check, this).setOnDismissListener({ir ->
                    finish()
                })
            }
            else {
                payment_message_fail.visibility = View.VISIBLE
                Popup.createPopup("Invalid Payment", R.drawable.error, this).setOnDismissListener({ir ->
                    finish()
                })
            }
        }
    }

    fun chequePayment(){

    }

    fun creditCardPayment(){

    }
}
