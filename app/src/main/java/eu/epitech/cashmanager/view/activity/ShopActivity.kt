package eu.epitech.cashmanager.view.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import eu.epitech.cashmanager.CashManagerApp
import eu.epitech.cashmanager.R
import eu.epitech.cashmanager.model.PaymentMethod
import eu.epitech.cashmanager.utils.Popup.Companion.createPopupSelectPayment
import kotlinx.android.synthetic.main.activity_shop.*


class ShopActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop)

        val myListAdapter = ShopListAdapter(this, CashManagerApp.instance.productList.list)
        shop_list.adapter = myListAdapter

        shop_list.setOnItemClickListener() { adapterView, view, position, id ->
            val itemAtPos = adapterView.getItemAtPosition(position)
            val itemIdAtPos = adapterView.getItemIdAtPosition(position)
            Toast.makeText(
                this,
                "Click on item at $itemAtPos its item id $itemIdAtPos",
                Toast.LENGTH_LONG
            ).show()
        }

        go_to_cart_btn.setOnClickListener({v ->
            createPopupSelectPayment(this).setOnDismissListener({ir ->
                if (CashManagerApp.instance.getPaymentMethod() != PaymentMethod.NULL) {
                    val intent = Intent(this, CartActivity::class.java)
                    startActivity(intent)
                }
            })
        })

        cancel_btn.setOnClickListener({ v ->
            CashManagerApp.instance.reset()
            finish();
            overridePendingTransition(0, 0);
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        })
        //createPopupSelectPayment();
    }


}
