package eu.epitech.cashmanager.utils

import android.content.Context

class Util {

    companion object {
        private val SHARED_PREFERENCES_NAME = "Preference"
        private val DEBUG_MODE = "DEBUG"
        private val TOKEN = "TOKEN"
        private val IP_SRV = "IP_SRV"
        private val ORDER = "ORDER_ID"




        fun persistDebugState(debug: Boolean, context: Context) {
            context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
                .putBoolean(DEBUG_MODE, debug)
                .commit()
        }

        fun getDebugState(context: Context): Boolean {
            return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .getBoolean(DEBUG_MODE, false)
        }

        fun persistOrderId(orderId : String, context: Context) {
            context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
                .putString(ORDER, orderId)
                .commit()
        }

        fun getOrderId(context: Context): String? {
            return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .getString(ORDER, "")
        }

        fun persistToken(token: String, context: Context) {
            context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
                .putString(TOKEN, token)
                .commit()
        }

        fun getToken(context: Context): String? {
            return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .getString(TOKEN, "")
        }

        fun persistServerIP(ip: String, context: Context) {
            context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
                .putString(IP_SRV, ip)
                .commit()
        }

        fun getServerIP(context: Context): String? {
            return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .getString(IP_SRV, "")
        }
    }

}