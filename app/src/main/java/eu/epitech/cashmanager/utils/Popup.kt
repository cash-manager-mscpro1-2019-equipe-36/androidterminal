package eu.epitech.cashmanager.utils

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import eu.epitech.cashmanager.CashManagerApp
import eu.epitech.cashmanager.R
import eu.epitech.cashmanager.model.PaymentMethod

class Popup {
    companion object {
        private val LOG_TAG = Popup::class.java.name
        fun createPopup(message: String, icon: Int, context: Context) : Dialog {
            Log.i(LOG_TAG, "createPopup with message$message")
            val dialog = Popup.getBaseDialog(context, R.layout.popup_default)

            val textView = dialog.findViewById<TextView>(eu.epitech.cashmanager.R.id.body)
            val buttonOk = dialog.findViewById<Button>(eu.epitech.cashmanager.R.id.buttonOk)
            val imageView = dialog.findViewById<ImageView>(eu.epitech.cashmanager.R.id.icon)

            imageView.setImageResource(icon)
            textView.setText(message)

            buttonOk.setOnClickListener({ v -> dialog.dismiss() })
            dialog.show()
            return dialog
        }

        fun createPopupSelectPayment(context: Context) : Dialog {
            val dialog = getBaseDialog(context, R.layout.popup_payment)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)
            val buttonCheque = dialog.findViewById<Button>(R.id.buttonCheque)
            val buttonCreditCard = dialog.findViewById<Button>(R.id.buttonCreditCard)

            buttonCheque.setOnClickListener({ v ->
                CashManagerApp.instance.setPaymentMethod(PaymentMethod.CHEQUE)
                dialog.dismiss()
            })
            buttonCreditCard.setOnClickListener({ v ->
                CashManagerApp.instance.setPaymentMethod(PaymentMethod.CREDIT_CARD)
                dialog.dismiss()
            })
            dialog.show()
            return dialog
        }

        private fun getBaseDialog(context: Context, layout: Int): Dialog {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(layout)
            dialog.setCanceledOnTouchOutside(false)
            dialog.setCancelable(false)

            return dialog
        }
    }
}