package eu.epitech.cashmanager.utils

import android.content.Context
import eu.epitech.cashmanager.CashManagerApp
import eu.epitech.cashmanager.R
import eu.epitech.cashmanager.model.OrdersRepository
import eu.epitech.cashmanager.model.Product
import java.security.AccessControlContext

class ProductList {

    var list: ArrayList<Product>

    constructor() {
        list = arrayListOf()
    }

    fun initListProduct() {
        list.clear()
        list.add(
            Product(
                "1",
                "Table",
                "C'est une très belle table",
                "https://www.atmosphera.com/phototheque/atmosphera.com/10500/large/01W010259A.jpg",
                0,
                5000
            )
        )
        list.add(
            Product(
                "2",
                "Chaise",
                "C'est une chaisse",
                "https://cdn.habitat.fr/thumbnails/product/842/842213/box/1200/1200/80/pippa-chaise-en-chene-naturel_842213.jpg",
                0,
                1000
            )
        )
        list.add(
            Product(
                "3",
                "Tabouret",
                "C'est une très belle table",
                "http://srv1.norskel.dev/images/i1.jpg",
                0,
                5000
            )
        )
    }

    fun addPoduct(id: Int,context: Context?) {
        OrdersRepository.getInstance().addProduct(list[id]) { isSuccess ->
            if (isSuccess)
                this.list[id].qty++
            else
                Popup.createPopup("Fail Add", R.drawable.error, context!!)
        }
    }

    fun subPoduct(id: Int,context: Context?) {
        if (this.list[id].qty > 0)
            OrdersRepository.getInstance().removeProduct(list[id]) { isSuccess ->
                if (isSuccess)
                    this.list[id].qty--
                else
                    Popup.createPopup("Fail delete", R.drawable.error, context!!)
            }
    }

    fun getTotal(): Int {
        var total = 0
        list.forEach { p ->
            total += (p.qty * p.price)
        }
        return total
    }

    fun getCartList(): ArrayList<Product> {
        var _list = this.list.clone() as ArrayList<Product>
        var i = 0

        do {
            if (_list[i].qty == 0) {
                _list.removeAt(i)
            } else {
                i++
            }
        } while (i < _list.size)
        return _list
    }

}