package eu.epitech.cashmanager.utils

class Constants {
    companion object {
        const val REQUEST_TIMEOUT_DURATION = 10
    }
}